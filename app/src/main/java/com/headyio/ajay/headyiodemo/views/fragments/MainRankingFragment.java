package com.headyio.ajay.headyiodemo.views.fragments;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.headyio.ajay.headyiodemo.AppContext;
import com.headyio.ajay.headyiodemo.AppExecutors;
import com.headyio.ajay.headyiodemo.R;
import com.headyio.ajay.headyiodemo.adapters.CustomSpinnerAdapter;
import com.headyio.ajay.headyiodemo.adapters.RankingSpinnerAdapter;
import com.headyio.ajay.headyiodemo.adapters.ResultAdapter;
import com.headyio.ajay.headyiodemo.databinding.FragmentProductListBinding;
import com.headyio.ajay.headyiodemo.db.AppDatabase;
import com.headyio.ajay.headyiodemo.db.entities.CategoryEntity;
import com.headyio.ajay.headyiodemo.db.entities.ProductEntity;
import com.headyio.ajay.headyiodemo.db.entities.RankingEntity;
import com.headyio.ajay.headyiodemo.db.entities.VariantsEntity;
import com.headyio.ajay.headyiodemo.interfaces.ApiInterface;
import com.headyio.ajay.headyiodemo.interfaces.ClickInterface;
import com.headyio.ajay.headyiodemo.utils.ApiClient;

import java.util.ArrayList;
import java.util.List;

public class MainRankingFragment extends Fragment implements ClickInterface {

    private FragmentProductListBinding binding;
    private ApiInterface apiService;
    private AppDatabase database;
    private AppExecutors appExecutors;
    private ResultAdapter resultAdapter;
    private List<ProductEntity> productsList = new ArrayList<>();
    private List<RankingEntity> rankingList = new ArrayList<>();
    private RankingSpinnerAdapter rankingSpinnerAdapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_product_list, container, false);

        View rootview = binding.getRoot();

        initResources();

        return rootview;

    }

    private void initResources() {
        apiService = ApiClient.getClient().create(ApiInterface.class);
        database = ((AppContext) getActivity().getApplicationContext()).getDatabase();
        appExecutors = new AppExecutors();

        binding.txtTitle.setText(getResources().getString(R.string.product_by_ranking));

        getDataFromDb();


        resultAdapter = new ResultAdapter(productsList, this);
        binding.recyclerview.setLayoutManager(new LinearLayoutManager(getActivity()));
        binding.recyclerview.setItemAnimator(new DefaultItemAnimator());
        binding.recyclerview.setAdapter(resultAdapter);
    }

    @Override
    public void setProduct(ProductEntity productEntity) {
        if (productEntity != null) {
            addFragment(new ProductDetailsFragment(), productEntity);
        }
    }

    private void getDataFromDb() {
        appExecutors.getExeDiskIO().execute(() -> {
            rankingList = new ArrayList<>();
            rankingList = database.getRankingDao().getRankingEntity();

            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    rankingSpinnerAdapter = new RankingSpinnerAdapter(getActivity(),
                            android.R.layout.simple_spinner_item,
                            rankingList);
                    binding.spinnerCategory.setAdapter(rankingSpinnerAdapter); // Set the custom adapter to the spinner
                    // You can create an anonymous listener to handle the event when is selected an spinner item
                    binding.spinnerCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view,
                                                   int position, long id) {
                            RankingEntity selectedRankingId = rankingSpinnerAdapter.getItem(position);
                            productsList = database.getRankingDao().getProductByRankId(selectedRankingId.getId());

                            if (productsList.size() > 0) {
                                resultAdapter.setProductsList(productsList);
                            }
                            resultAdapter.notifyDataSetChanged();
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapter) {
                        }
                    });
                }
            });

        });


    }

    private void addFragment(Fragment fragment, ProductEntity productEntity) {
        Bundle bundle = new Bundle();
        bundle.putInt("productId", productEntity.getId());
        bundle.putString("productName", productEntity.getName());
        fragment.setArguments(bundle);
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frameLayout_main, fragment, "ProductDetailsFragment").addToBackStack(null);
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.commitAllowingStateLoss();
    }
}
