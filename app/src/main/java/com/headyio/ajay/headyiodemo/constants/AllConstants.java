package com.headyio.ajay.headyiodemo.constants;

/**
 * Created by root on 26/4/18.
 */

public interface AllConstants {
    int    DATABASE_VERSION       = 1;
    String DATABASE_NAME          = "headyiodemo.db";
    String TABLE_CATEGORY         = "Category";
    String TABLE_CHILD_CATEGORY   = "ChildCategory";
    String TABLE_TAX              = "Tax";
    String TABLE_TAX_MAPPING      = "TaxProductMapping";
    String TABLE_RANKING          = "Ranking";
    String TABLE_RANKING_MAPPING  = "RankingProductMapping";
    String TABLE_VARIANT          = "Variant";
    String TABLE_VARIANT_MAPPING  = "VariantProductMapping";
    String TABLE_PRODUCT          = "Product";
    String TABLE_DB_STATUS        = "DBStatus";

}
