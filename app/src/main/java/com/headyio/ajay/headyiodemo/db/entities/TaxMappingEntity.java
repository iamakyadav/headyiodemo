package com.headyio.ajay.headyiodemo.db.entities;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import static com.headyio.ajay.headyiodemo.constants.AllConstants.TABLE_TAX;
import static com.headyio.ajay.headyiodemo.constants.AllConstants.TABLE_TAX_MAPPING;

@Entity(tableName = TABLE_TAX_MAPPING)
public class TaxMappingEntity {

    @PrimaryKey(autoGenerate = true)
    private int id;
    private int productId;
    private int taxId;

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getTaxId() {
        return taxId;
    }

    public void setTaxId(int taxId) {
        this.taxId = taxId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
