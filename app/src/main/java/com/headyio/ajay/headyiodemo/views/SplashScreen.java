package com.headyio.ajay.headyiodemo.views;

import android.app.Activity;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.AppCompatTextView;
import android.util.Log;
import android.view.View;

import com.headyio.ajay.headyiodemo.AppContext;
import com.headyio.ajay.headyiodemo.AppExecutors;
import com.headyio.ajay.headyiodemo.R;
import com.headyio.ajay.headyiodemo.databinding.ActivitySplashScreenBinding;
import com.headyio.ajay.headyiodemo.db.AppDatabase;
import com.headyio.ajay.headyiodemo.db.entities.CategoryEntity;
import com.headyio.ajay.headyiodemo.db.entities.ChildCategoryEntity;
import com.headyio.ajay.headyiodemo.db.entities.DBStatusEntity;
import com.headyio.ajay.headyiodemo.db.entities.ProductEntity;
import com.headyio.ajay.headyiodemo.db.entities.RankingEntity;
import com.headyio.ajay.headyiodemo.db.entities.RankingMappingEntity;
import com.headyio.ajay.headyiodemo.db.entities.TaxEntity;
import com.headyio.ajay.headyiodemo.db.entities.VariantsEntity;
import com.headyio.ajay.headyiodemo.db.entities.VariantsMappingEntity;
import com.headyio.ajay.headyiodemo.interfaces.ApiInterface;
import com.headyio.ajay.headyiodemo.models.AllData;
import com.headyio.ajay.headyiodemo.models.CategoriesModel;
import com.headyio.ajay.headyiodemo.models.ProductsModel;
import com.headyio.ajay.headyiodemo.models.RankingModel;
import com.headyio.ajay.headyiodemo.models.RankingProductModel;
import com.headyio.ajay.headyiodemo.models.TaxModel;
import com.headyio.ajay.headyiodemo.models.VariantsModel;
import com.headyio.ajay.headyiodemo.utils.ApiClient;
import com.headyio.ajay.headyiodemo.utils.ApplicationUtils;

import java.util.Iterator;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

import static android.content.ContentValues.TAG;

/**
 * Created by Nihar.s on 30/8/18.
 */


public class SplashScreen extends Activity {


    /**
     * Called when the activity is first created.
     */

    private ActivitySplashScreenBinding binding;

    private AllData allData;
    private Call<AllData> call_resource;
    private ApiInterface apiService;
    private AppDatabase database;
    private AppExecutors appExecutors;

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_splash_screen);

        binding.typeWriter.setCharacterDelay(40);
        binding.typeWriter.animateText(getResources().getString(R.string.app_name));

        binding.progressbar.setVisibility(View.VISIBLE);

        initResource();
    }


    private void initResource() {

        apiService = ApiClient.getClient().create(ApiInterface.class);
        database = ((AppContext) getApplicationContext()).getDatabase();
        appExecutors = new AppExecutors();
        if (ApplicationUtils.isNetworkConnected(SplashScreen.this)) {
            binding.btnTryAgain.setVisibility(View.GONE);
            getDataFromServer();

        } else {
            binding.btnTryAgain.setVisibility(View.VISIBLE);
        }

        binding.btnTryAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ApplicationUtils.isNetworkConnected(SplashScreen.this)) {
                    getDataFromServer();
                }
            }
        });

    }

    private void getDataFromServer() {

        appExecutors.getExeDiskIO().execute(() -> {
            DBStatusEntity dbStatusEntity = database.getDBDbStatusDao().getDBStatus();
            if (dbStatusEntity == null) {
                DBStatusEntity dbStatusEntity1 = new DBStatusEntity();
                dbStatusEntity1.setStatus(0);
                database.getDBDbStatusDao().insertDBStatus(dbStatusEntity1);
                getProductList();
            } else {
                DBStatusEntity dbStatusEntity12 = database.getDBDbStatusDao().getDBStatus();
                if (dbStatusEntity12.getStatus() == 0) {
                    clearAllData();
                } else {
                    navigateToNextScreen();
                }
            }
        });
    }


    public void getProductList() {

        call_resource = apiService.getAllData();

        call_resource.enqueue(new Callback<AllData>() {
            @Override
            public void onResponse(Call<AllData> call, retrofit2.Response<AllData> response) {

                if (response != null && response.code() == 200) {
                    allData = response.body();

                    CategoriesModel categoryModel = null;
                    Iterator<CategoriesModel> iteratorCategory = allData.categories.iterator();
                    while (iteratorCategory.hasNext()) {
                        categoryModel = iteratorCategory.next();
                        insertCategoryData(categoryModel);
                        insertChildCategoryData(categoryModel);
                        iteratorCategory.remove();
                    }


                    RankingModel rankingModel = null;
                    Iterator<RankingModel> iteratorRanking = allData.rankings.iterator();
                    while (iteratorRanking.hasNext()) {
                        rankingModel = iteratorRanking.next();
                        insertRankingData(rankingModel);
                        iteratorRanking.remove();
                    }

                    navigateToNextScreen();
                } else {


                }
            }


            @Override
            public void onFailure(Call<AllData> call, Throwable t) {
                Log.e("status", "onFailure: ");
            }
        });
    }


    private void insertCategoryData(CategoriesModel categoriesModel) {
        CategoryEntity categoryEntity = new CategoryEntity();
        categoryEntity.setId(categoriesModel.getId());
        categoryEntity.setName(categoriesModel.getName());


        database.getCategoryDao().insertCategory(categoryEntity);

        ProductsModel productsModel = null;
        Iterator<ProductsModel> iterator = categoriesModel.products.iterator();
        while (iterator.hasNext()) {
            productsModel = iterator.next();
            startInsertVariantProcess(productsModel, categoryEntity.getId());
            iterator.remove();
        }


    }

    private void startInsertVariantProcess(ProductsModel productsModel, int catId) {

        List<VariantsModel> variantsList = productsModel.variants;
        int productId = productsModel.id;
        VariantsModel variantsModel = null;
        Iterator<VariantsModel> iterator = variantsList.iterator();
        while (iterator.hasNext()) {
            variantsModel = iterator.next();
            VariantsEntity variantsEntity = new VariantsEntity();
            variantsEntity.setId(variantsModel.getId());
            variantsEntity.setColor(variantsModel.getColor());
            variantsEntity.setSize(variantsModel.getSize());
            variantsEntity.setPrice(variantsModel.getPrice());
            insertVariantData(variantsEntity, productId);
            iterator.remove();
        }

        insertTaxData(productsModel, catId);


    }

    private void insertVariantData(VariantsEntity variantsEntity, int productId) {


        database.getVariantsDao().insertVariant(variantsEntity);

        VariantsMappingEntity variantsMappingEntity = new VariantsMappingEntity();
        variantsMappingEntity.setProductId(productId);
        variantsMappingEntity.setVariantId(variantsEntity.getId());
        insertVariantProductMapping(variantsMappingEntity);
    }

    private void insertVariantProductMapping(VariantsMappingEntity variantsMappingEntity) {
        appExecutors.getExeDiskIO().execute(() -> {
            database.getVariantsMappingDao().insertVariantProductMapping(variantsMappingEntity);
        });
    }

    private void insertTaxData(ProductsModel productsModel, int catId) {
        TaxModel taxModel = productsModel.tax;
        TaxEntity taxEntity = database.getTaxDao().getTax(taxModel.getName(), taxModel.getValue());
        final long[] insertedID = {0};
        if (taxEntity == null) {
            TaxEntity taxEntity12 = new TaxEntity();
            taxEntity12.setName(taxModel.getName());
            taxEntity12.setValue(taxModel.getValue());
            insertedID[0] = database.getTaxDao().insertTax(taxEntity12);
        } else {
            insertedID[0] = taxEntity.getId();
        }

        ProductEntity productEntity = new ProductEntity();
        productEntity.setId(productsModel.id);
        productEntity.setCategoryId(catId);
        productEntity.setName(productsModel.getName());
        productEntity.setDate_added(productsModel.getDate_added());
        productEntity.setTaxId((int) insertedID[0]);


        database.getProductDao().insertProduct(productEntity);


        Log.i(TAG, "insertTaxData: " + productEntity.getName());
    }

    private void insertRankingData(RankingModel rankingModel) {
        RankingEntity rankingEntity = new RankingEntity();
        rankingEntity.setName(rankingModel.getRanking());
        final long[] insertedID = {0};

        insertedID[0] = database.getRankingDao().insertRanking(rankingEntity);

        RankingProductModel rankingProductModel = null;
        Iterator<RankingProductModel> iterator = rankingModel.getProducts().iterator();
        while (iterator.hasNext()) {
            rankingProductModel = iterator.next();
            if (rankingProductModel.order_count != 0) {
                database.getProductDao().updateProductOrderCount(rankingProductModel.id, rankingProductModel.order_count);
            } else if (rankingProductModel.shares != 0) {
                database.getProductDao().updateProductShareCount(rankingProductModel.id, rankingProductModel.shares);
            } else if (rankingProductModel.view_count != 0) {
                database.getProductDao().updateProductViewCount(rankingProductModel.id, rankingProductModel.view_count);
            }

            RankingMappingEntity rankingMappingEntity = new RankingMappingEntity();
            rankingMappingEntity.setProductId(rankingProductModel.id);
            rankingMappingEntity.setRankId((int) insertedID[0]);
            database.getRankingMappingDao().insertRankMapping(rankingMappingEntity);

            iterator.remove();
        }
        database.getDBDbStatusDao().updateDBSTatus(1);

    }

    private void clearAllData() {

        database.getCategoryDao().clearCategoryData();
        database.getChildCategoryDao().clearChildCategoryData();
        database.getProductDao().clearProductData();
        database.getRankingDao().clearRankingData();
        database.getRankingMappingDao().clearRankingMappingData();
        database.getTaxDao().clearTaxData();
        database.getTaxMappingDao().clearTaxMappingData();
        database.getVariantsDao().clearVariantData();
        database.getVariantsMappingDao().clearVariantMappingData();
        getProductList();

    }

    private void insertChildCategoryData(CategoriesModel categoryModel) {
        List<Integer> childCategory = categoryModel.child_categories;
        for (int child : childCategory) {
            ChildCategoryEntity childCategoryEntity = new ChildCategoryEntity();
            childCategoryEntity.setCategoryID(categoryModel.getId());
            childCategoryEntity.setChildCategoryID(child);

            database.getChildCategoryDao().insertChildCategory(childCategoryEntity);
        }
    }


    private void navigateToNextScreen() {

        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                binding.progressbar.setVisibility(View.GONE);
            }
        });


        ApplicationUtils.simpleIntentFinish(this, HomeActivity.class, Bundle.EMPTY);
    }

}