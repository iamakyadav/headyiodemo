package com.headyio.ajay.headyiodemo.db.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import com.headyio.ajay.headyiodemo.db.entities.DBStatusEntity;
import static com.headyio.ajay.headyiodemo.constants.AllConstants.*;

@Dao
public interface DBStatusDao {

    @Query("Select * from " + TABLE_DB_STATUS)
    DBStatusEntity getDBStatus();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertDBStatus(DBStatusEntity dbStatusEntity);

    /*status 0=Empty or All data not inserted; 1=All data inserted*/
    @Query("Update " + TABLE_DB_STATUS + " set status=:status")
    int updateDBSTatus(int status);

}
