package com.headyio.ajay.headyiodemo.views.fragments;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.headyio.ajay.headyiodemo.AppContext;
import com.headyio.ajay.headyiodemo.AppExecutors;
import com.headyio.ajay.headyiodemo.R;
import com.headyio.ajay.headyiodemo.adapters.ResultAdapter;
import com.headyio.ajay.headyiodemo.adapters.VarientProductAdapter;
import com.headyio.ajay.headyiodemo.databinding.FragmentProductDetailsBinding;
import com.headyio.ajay.headyiodemo.db.AppDatabase;
import com.headyio.ajay.headyiodemo.db.entities.ProductEntity;
import com.headyio.ajay.headyiodemo.db.entities.VariantsEntity;
import com.headyio.ajay.headyiodemo.interfaces.ApiInterface;
import com.headyio.ajay.headyiodemo.utils.ApiClient;

import java.util.ArrayList;
import java.util.List;


public class ProductDetailsFragment extends Fragment {


    private FragmentProductDetailsBinding binding;
    private AppDatabase database;
    private AppExecutors appExecutors;
    private ApiInterface apiService;
    private List<VariantsEntity> variantsEntitiesList = new ArrayList<>();
    private VarientProductAdapter varientProductAdapter;
    private int productId = 0;
    private String productName = "";


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_product_details, container, false);

        View rootview = binding.getRoot();

        initResources();

        return rootview;

    }

    public ProductDetailsFragment() {

    }


    private void initResources() {
        Bundle bundle = getArguments();

        if (bundle != null) {
            productId = bundle.getInt("productId");
            productName = bundle.getString("productName");
        }

        binding.toolbar.txtToolbarTitle.setText(productName);
        apiService = ApiClient.getClient().create(ApiInterface.class);
        database = ((AppContext) getActivity().getApplicationContext()).getDatabase();

        varientProductAdapter = new VarientProductAdapter(variantsEntitiesList);
        binding.recyclerview.setLayoutManager(new LinearLayoutManager(getActivity()));
        binding.recyclerview.setItemAnimator(new DefaultItemAnimator());
        binding.recyclerview.setAdapter(varientProductAdapter);


        appExecutors = new AppExecutors();
        appExecutors.getExeDiskIO().execute(() -> {

            variantsEntitiesList = database.getVariantsDao().getvariantProductByproductId(productId);


            if (variantsEntitiesList.size() > 0) {
                varientProductAdapter.setvarientList(variantsEntitiesList);
            }

            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    varientProductAdapter.notifyDataSetChanged();
                }
            });

        });


    }
}
