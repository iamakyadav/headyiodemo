package com.headyio.ajay.headyiodemo.db.entities;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.headyio.ajay.headyiodemo.db.dao.CategoryDao;

import java.util.List;

import static com.headyio.ajay.headyiodemo.constants.AllConstants.*;

@Entity(tableName = TABLE_CATEGORY)
public class CategoryEntity{

    @PrimaryKey
    private int id;
    private String name;

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
