package com.headyio.ajay.headyiodemo.db.entities;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import static com.headyio.ajay.headyiodemo.constants.AllConstants.TABLE_CATEGORY;
import static com.headyio.ajay.headyiodemo.constants.AllConstants.TABLE_CHILD_CATEGORY;

@Entity(tableName = TABLE_CHILD_CATEGORY)
public class ChildCategoryEntity {

    @PrimaryKey(autoGenerate = true)
    private int id;
    private int categoryID;
    private int childCategoryID;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCategoryID() {
        return categoryID;
    }

    public void setCategoryID(int categoryID) {
        this.categoryID = categoryID;
    }

    public int getChildCategoryID() {
        return childCategoryID;
    }

    public void setChildCategoryID(int childCategoryID) {
        this.childCategoryID = childCategoryID;
    }
}
