package com.headyio.ajay.headyiodemo.db.entities;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;

import static com.headyio.ajay.headyiodemo.constants.AllConstants.*;

/*

@Entity(tableName = "chat_message_table",
        indices = @Index(value = {"messageId"}, unique = true))
*/


@Entity(tableName = TABLE_TAX, indices = @Index(value = {"name"}, unique = true))
public class TaxEntity {


    @PrimaryKey(autoGenerate = true)
    private int id;
    private String name;
    private double value;

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }
}
