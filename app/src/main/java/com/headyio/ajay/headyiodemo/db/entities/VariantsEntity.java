package com.headyio.ajay.headyiodemo.db.entities;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import static com.headyio.ajay.headyiodemo.constants.AllConstants.TABLE_TAX;
import static com.headyio.ajay.headyiodemo.constants.AllConstants.TABLE_VARIANT;

@Entity(tableName = TABLE_VARIANT)
public class VariantsEntity {
    @PrimaryKey
    private int id;
    private String color;
    private int size;
    private double price;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
