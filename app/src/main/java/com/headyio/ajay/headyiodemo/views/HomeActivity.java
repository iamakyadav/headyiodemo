package com.headyio.ajay.headyiodemo.views;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.headyio.ajay.headyiodemo.R;
import com.headyio.ajay.headyiodemo.db.entities.CategoryEntity;
import com.headyio.ajay.headyiodemo.viewModels.CategoryViewModel;
import com.headyio.ajay.headyiodemo.views.fragments.HomeFragment;

import java.util.List;

public class HomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

       /* AppExecutors appExecutors = new AppExecutors();
        appExecutors.getExeDiskIO().execute(()->{
            AppContext context = (AppContext) AppContext.getInstance();
            AppDatabase appDatabase = context.getDatabase();
            CategoryEntity entity = new CategoryEntity();
           // entity.setId((int)Math.random());
            entity.setName("Test");
            appDatabase.getCategoryDao().insertCategory(entity);

            List<CategoryEntity> list = appDatabase.getCategoryDao().getCategoryEntityList();
            Log.e("Value",""+list.size());
        });*/

    /*    final CategoryViewModel viewModel =
                ViewModelProviders.of(this).get(CategoryViewModel.class);
        onSubscribeUI(viewModel);*/

        addFragment(new HomeFragment());
    }

    private void onSubscribeUI(CategoryViewModel viewModel) {
        List<CategoryEntity> list = viewModel.getCategoryList();
    }

    private void addFragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frameLayout_main, fragment, "CharacterListFragment").addToBackStack(null);
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.commitAllowingStateLoss();
    }


    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() == 1) {
            finish();
        } else if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
            getSupportFragmentManager().popBackStack();
        }

    }

}
