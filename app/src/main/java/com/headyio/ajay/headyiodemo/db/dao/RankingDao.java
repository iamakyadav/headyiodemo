package com.headyio.ajay.headyiodemo.db.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.headyio.ajay.headyiodemo.db.entities.ProductEntity;
import com.headyio.ajay.headyiodemo.db.entities.RankingEntity;
import com.headyio.ajay.headyiodemo.models.RankingDisplayModel;

import java.util.List;

import static com.headyio.ajay.headyiodemo.constants.AllConstants.*;

@Dao
public interface RankingDao {

    @Query("Select * from " + TABLE_RANKING)
    List<RankingEntity> getRankingEntity();

    @Insert()
    long insertRanking(RankingEntity rankingEntity);


    @Query("Delete from " + TABLE_RANKING)
    void clearRankingData();


    @Query("select * from " + TABLE_PRODUCT + " where id in (select productId from " + TABLE_RANKING_MAPPING + " where rankId=:rankId)")
    List<ProductEntity> getProductByRankId(int rankId);

}
