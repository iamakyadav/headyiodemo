package com.headyio.ajay.headyiodemo.models;

import com.headyio.ajay.headyiodemo.db.entities.ProductEntity;

import java.util.List;

public class RankingDisplayModel {

    public String rankingName;

    public List<ProductEntity> getProductList() {
        return productList;
    }

    public void setProductList(List<ProductEntity> productList) {
        this.productList = productList;
    }

    List<ProductEntity> productList;

    public String getRankingName() {
        return rankingName;
    }

    public void setRankingName(String rankingName) {
        this.rankingName = rankingName;
    }


}
