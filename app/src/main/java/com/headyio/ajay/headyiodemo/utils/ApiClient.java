package com.headyio.ajay.headyiodemo.utils;

import android.util.Log;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class ApiClient {


    static OkHttpClient.Builder builder = new OkHttpClient.Builder();
    static OkHttpClient client = new OkHttpClient.Builder()
            .readTimeout(18000, TimeUnit.SECONDS)
            .connectTimeout(18000, TimeUnit.SECONDS)
            .build();
    private static Retrofit retrofit = null;

    Interceptor interceptor = new Interceptor() {
        @Override
        public okhttp3.Response intercept(Chain chain) throws IOException {
            Request newRequest = chain.request().newBuilder().build();
            return chain.proceed(newRequest);
        }
    };


    public static Retrofit getClient() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl("https://stark-spire-93433.herokuapp.com/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(client)
                    .build();
            Log.i("retrofit", "getClient: " + retrofit);
        }
        return retrofit;
    }
}
