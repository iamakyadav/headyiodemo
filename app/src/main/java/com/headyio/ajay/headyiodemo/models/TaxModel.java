package com.headyio.ajay.headyiodemo.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TaxModel {

    private double value;

    private String name;

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
