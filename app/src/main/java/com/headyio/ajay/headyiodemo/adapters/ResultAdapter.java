package com.headyio.ajay.headyiodemo.adapters;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.headyio.ajay.headyiodemo.R;
import com.headyio.ajay.headyiodemo.databinding.TemplateListBinding;
import com.headyio.ajay.headyiodemo.db.entities.ProductEntity;
import com.headyio.ajay.headyiodemo.interfaces.ClickInterface;

import java.util.List;


public class ResultAdapter extends RecyclerView.Adapter<ResultAdapter.MyViewHolder> {

    private List<ProductEntity> productList;
    private LayoutInflater layoutInflater;
    private Context context;
    private ClickInterface clickInterface;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private final TemplateListBinding binding;

        public MyViewHolder(TemplateListBinding itemBinding) {
            super(itemBinding.getRoot());
            this.binding = itemBinding;
        }
    }

    public ResultAdapter(List<ProductEntity> productList, ClickInterface clickInterface) {
        this.productList = productList;
        this.clickInterface = clickInterface;
    }


    public void setProductsList(List<ProductEntity> starWarsList) {
        this.productList = starWarsList;
    }


    @Override
    public ResultAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.getContext());
        }
        context = parent.getContext();

        TemplateListBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.template_list, parent, false);
        return new MyViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(ResultAdapter.MyViewHolder holder, final int position) {
        holder.binding.setProduct(productList.get(position));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickInterface.setProduct(productList.get(position));

            }
        });
    }

    @Override
    public int getItemCount() {
        return productList.size();
    }
}
