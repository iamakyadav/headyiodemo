package com.headyio.ajay.headyiodemo.db.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;

import com.headyio.ajay.headyiodemo.db.entities.TaxMappingEntity;

import java.util.List;

import static com.headyio.ajay.headyiodemo.constants.AllConstants.*;

@Dao
public interface TaxMappingDao {
    @Query("Select * from "+TABLE_TAX_MAPPING)
    List<TaxMappingEntity> getTaxMappingEntity();

    @Query("Delete from " + TABLE_TAX_MAPPING)
    void clearTaxMappingData();

}
