package com.headyio.ajay.headyiodemo.models;


import java.util.List;

public class ProductsModel {

    public TaxModel tax;

    public List<VariantsModel> variants;

    public String date_added;

    public String name;

    public int id;

    public TaxModel getTax() {
        return tax;
    }

    public void setTax(TaxModel tax) {
        this.tax = tax;
    }

    public List<VariantsModel> getVariants() {
        return variants;
    }

    public void setVariants(List<VariantsModel> variants) {
        this.variants = variants;
    }

    public String getDate_added() {
        return date_added;
    }

    public void setDate_added(String date_added) {
        this.date_added = date_added;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
