package com.headyio.ajay.headyiodemo.viewModels;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import com.headyio.ajay.headyiodemo.AppContext;
import com.headyio.ajay.headyiodemo.DataRepository;
import com.headyio.ajay.headyiodemo.db.entities.CategoryEntity;
import java.util.ArrayList;
import java.util.List;

public class CategoryViewModel extends AndroidViewModel {

    private final  List<CategoryEntity> entityList;
    private DataRepository dataRepository;

    public CategoryViewModel(Application application) {
        super(application);
        dataRepository = ((AppContext) application).getDataRepository();
        entityList = dataRepository.getCategoryList();
    }

    public List<CategoryEntity>  getCategoryList() {
        return entityList;
    }



}
