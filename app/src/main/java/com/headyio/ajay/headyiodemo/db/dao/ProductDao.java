package com.headyio.ajay.headyiodemo.db.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.headyio.ajay.headyiodemo.db.entities.ProductEntity;

import java.util.List;

import static com.headyio.ajay.headyiodemo.constants.AllConstants.*;

@Dao
public interface ProductDao {
    @Query("Select * from " + TABLE_PRODUCT)
    List<ProductEntity> getProductEntity();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertProduct(ProductEntity productEntity);

    @Query("Update " + TABLE_PRODUCT + " set viewCount=:count where id=:Id")
    int updateProductViewCount(int Id, int count);

    @Query("Update " + TABLE_PRODUCT + " set sharesCount=:count where id=:Id")
    int updateProductShareCount(int Id, int count);


    @Query("Update " + TABLE_PRODUCT + " set orderCount=:count where id=:Id")
    int updateProductOrderCount(int Id, int count);

    @Query("Select * from " + TABLE_PRODUCT + " where categoryId in (:catId)")
    List<ProductEntity> getProductsByCatId(int[] catId);


    @Query("Delete from " + TABLE_PRODUCT)
    void clearProductData();


}
