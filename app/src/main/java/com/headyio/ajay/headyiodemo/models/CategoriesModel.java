package com.headyio.ajay.headyiodemo.models;


import java.util.List;

public class CategoriesModel {

    public List<Integer> child_categories;

    public List<ProductsModel> products;

    public String name;

    public int id;

    public List<Integer> getChild_categories() {
        return child_categories;
    }

    public void setChild_categories(List<Integer> child_categories) {
        this.child_categories = child_categories;
    }

    public List<ProductsModel> getProducts() {
        return products;
    }

    public void setProducts(List<ProductsModel> products) {
        this.products = products;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
