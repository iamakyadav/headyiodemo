package com.headyio.ajay.headyiodemo.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VariantsModel {

    public int price;

    public int size;

    public String color;

    public int id;

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
