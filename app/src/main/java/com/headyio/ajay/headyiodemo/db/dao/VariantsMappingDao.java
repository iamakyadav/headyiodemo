package com.headyio.ajay.headyiodemo.db.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.headyio.ajay.headyiodemo.db.entities.VariantsMappingEntity;

import java.util.List;

import static com.headyio.ajay.headyiodemo.constants.AllConstants.*;

@Dao
public interface VariantsMappingDao {
    @Query("Select * from "+TABLE_VARIANT_MAPPING)
    List<VariantsMappingEntity> getVariantsMappingEntity();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertVariantProductMapping(VariantsMappingEntity variantsMappingEntity);

    @Query("Delete from " + TABLE_VARIANT_MAPPING)
    void clearVariantMappingData();

}
