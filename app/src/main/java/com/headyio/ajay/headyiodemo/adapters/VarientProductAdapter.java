package com.headyio.ajay.headyiodemo.adapters;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.headyio.ajay.headyiodemo.R;
import com.headyio.ajay.headyiodemo.databinding.TemplateVariantBinding;
import com.headyio.ajay.headyiodemo.db.entities.ProductEntity;
import com.headyio.ajay.headyiodemo.db.entities.VariantsEntity;

import java.util.List;

public class VarientProductAdapter extends RecyclerView.Adapter<VarientProductAdapter.MyViewHolder> {

    private List<VariantsEntity> varientList;
    private LayoutInflater layoutInflater;
    private Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public final TemplateVariantBinding binding;

        public MyViewHolder(TemplateVariantBinding itemBinding) {
            super(itemBinding.getRoot());
            this.binding = itemBinding;
        }
    }

    public VarientProductAdapter(List<VariantsEntity> varientList) {
        this.varientList = varientList;

    }


    public void setvarientList(List<VariantsEntity> varientList) {
        this.varientList = varientList;
    }


    @Override
    public VarientProductAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.getContext());
        }
        context = parent.getContext();

        TemplateVariantBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.template_variant, parent, false);
        return new VarientProductAdapter.MyViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(VarientProductAdapter.MyViewHolder holder, final int position) {

        if (position > 0) {
            holder.binding.tvColor.setText(varientList.get(position - 1).getColor());
            holder.binding.tvSize.setText(String.valueOf(varientList.get(position - 1).getSize()));
            holder.binding.tvPrice.setText(String.valueOf(varientList.get(position - 1).getPrice()));
        }

    }


    @Override
    public int getItemCount() {
        return varientList.size() + 1;
    }


}
