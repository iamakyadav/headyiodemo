package com.headyio.ajay.headyiodemo.db.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.headyio.ajay.headyiodemo.db.entities.CategoryEntity;
import com.headyio.ajay.headyiodemo.models.CategoriesModel;

import java.util.List;

import static com.headyio.ajay.headyiodemo.constants.AllConstants.*;

@Dao
public interface CategoryDao {
    @Query("Select * from " + TABLE_CATEGORY)
    LiveData<List<CategoryEntity>> getCategoryEntity();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertCategory(CategoryEntity entity);

    @Query("Select * from " + TABLE_CATEGORY)
    List<CategoryEntity> getCategoryEntityList();


    @Query("Delete from " + TABLE_CATEGORY)
    void clearCategoryData();



}
