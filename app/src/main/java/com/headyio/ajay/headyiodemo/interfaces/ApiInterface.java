package com.headyio.ajay.headyiodemo.interfaces;


import com.headyio.ajay.headyiodemo.models.AllData;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;


public interface ApiInterface {

    @GET("json")
    Call<AllData> getAllData();
}

