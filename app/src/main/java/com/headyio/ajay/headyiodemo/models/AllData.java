package com.headyio.ajay.headyiodemo.models;

import java.util.List;

public class AllData {

    public List<CategoriesModel> categories;
    public List<RankingModel> rankings;

}
