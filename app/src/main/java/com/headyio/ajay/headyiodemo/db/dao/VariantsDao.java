package com.headyio.ajay.headyiodemo.db.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.headyio.ajay.headyiodemo.db.entities.CategoryEntity;
import com.headyio.ajay.headyiodemo.db.entities.ProductEntity;
import com.headyio.ajay.headyiodemo.db.entities.VariantsEntity;

import java.util.List;

import static com.headyio.ajay.headyiodemo.constants.AllConstants.*;

@Dao
public interface VariantsDao {
    @Query("Select * from "+TABLE_VARIANT)
    List<VariantsEntity> getVariantsEntity();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertVariant(VariantsEntity variantsEntity);

    @Query("Delete from " + TABLE_VARIANT)
    void clearVariantData();

    @Query("select * from Variant left join VariantProductMapping on VariantProductMapping.variantId=Variant.id where VariantProductMapping.productId=:pId")
    List<VariantsEntity> getvariantProductByproductId(int pId);

}
