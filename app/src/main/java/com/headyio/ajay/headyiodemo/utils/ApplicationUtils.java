package com.headyio.ajay.headyiodemo.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;

public class ApplicationUtils {

    private static int TYPE_NOT_CONNECTED = -1;

    public static void startActivityIntent(Context context, Class aClass, Bundle bundle) {
        Intent intent = new Intent(context, aClass);
        intent.putExtras(bundle);
        context.startActivity(intent);
    }


    public static void simpleIntentFinish(Activity activity, Class aClass, Bundle bundle) {
        Intent intent = new Intent(activity, aClass);
        intent.putExtras(bundle);
        activity.startActivity(intent);
        activity.finish();

    }


    public static boolean isNetworkConnected(Context context) {
        int connectivityType = getConnectivityType(context);
        if (connectivityType == TYPE_NOT_CONNECTED)
            return false;
        else
            return true;
    }

    public static int getConnectivityType(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        if (null != networkInfo && networkInfo.isConnected()) {
            return networkInfo.getType();
        }
        return TYPE_NOT_CONNECTED;
    }

}
