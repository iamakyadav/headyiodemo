package com.headyio.ajay.headyiodemo;

import android.app.Application;
import android.content.Context;

import com.headyio.ajay.headyiodemo.db.AppDatabase;

public class AppContext extends Application {
    private AppExecutors   appExecutors;
    private static Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        appExecutors = new AppExecutors();
        context = getApplicationContext();
        getDataRepository();
    }

    public AppDatabase getDatabase(){
        return AppDatabase.getInstance(this, appExecutors);
    }

    public DataRepository getDataRepository(){
        return DataRepository.getDataRepository(getDatabase());
    }

    public static Context getInstance(){
        return AppContext.context;
    }

}