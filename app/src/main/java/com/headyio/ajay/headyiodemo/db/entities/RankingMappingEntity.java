package com.headyio.ajay.headyiodemo.db.entities;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import static com.headyio.ajay.headyiodemo.constants.AllConstants.TABLE_RANKING;
import static com.headyio.ajay.headyiodemo.constants.AllConstants.TABLE_RANKING_MAPPING;

@Entity(tableName = TABLE_RANKING_MAPPING)
public class RankingMappingEntity {
    @PrimaryKey(autoGenerate = true)
    private int id;
    private int productId;
    private int rankId;

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getRankId() {
        return rankId;
    }

    public void setRankId(int rankId) {
        this.rankId = rankId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
