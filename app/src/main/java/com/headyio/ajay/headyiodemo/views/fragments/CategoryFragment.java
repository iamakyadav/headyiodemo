package com.headyio.ajay.headyiodemo.views.fragments;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.headyio.ajay.headyiodemo.AppContext;
import com.headyio.ajay.headyiodemo.AppExecutors;
import com.headyio.ajay.headyiodemo.R;
import com.headyio.ajay.headyiodemo.adapters.CustomSpinnerAdapter;
import com.headyio.ajay.headyiodemo.adapters.ResultAdapter;
import com.headyio.ajay.headyiodemo.databinding.FragmentProductListBinding;
import com.headyio.ajay.headyiodemo.db.AppDatabase;
import com.headyio.ajay.headyiodemo.db.entities.CategoryEntity;
import com.headyio.ajay.headyiodemo.db.entities.ProductEntity;
import com.headyio.ajay.headyiodemo.interfaces.ApiInterface;
import com.headyio.ajay.headyiodemo.interfaces.ClickInterface;
import com.headyio.ajay.headyiodemo.utils.ApiClient;

import java.util.ArrayList;
import java.util.List;


public class CategoryFragment extends Fragment implements ClickInterface {

    private FragmentProductListBinding binding;
    private ApiInterface apiService;
    private AppDatabase database;
    private AppExecutors appExecutors;
    private List<CategoryEntity> categoryList;
    private CustomSpinnerAdapter customSpinnerAdapter, childCustomSpinnerAdapter;
    private ResultAdapter resultAdapter;
    private List<ProductEntity> productsList = new ArrayList<>();
    private ClickInterface clickInterface;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_product_list, container, false);

        View rootview = binding.getRoot();

        initResources();

        return rootview;
    }

    private void initResources() {

        apiService = ApiClient.getClient().create(ApiInterface.class);
        database = ((AppContext) getActivity().getApplicationContext()).getDatabase();
        appExecutors = new AppExecutors();

        binding.txtTitle.setText(getResources().getString(R.string.product_by_category));

        clickInterface = this;
        getDataFromDb();

    }


    private void getDataFromDb() {
        appExecutors.getExeDiskIO().execute(() -> {
            categoryList = new ArrayList<>();
            categoryList = database.getCategoryDao().getCategoryEntityList();

            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    customSpinnerAdapter = new CustomSpinnerAdapter(getActivity(),
                            android.R.layout.simple_spinner_item,
                            categoryList);
                    binding.spinnerCategory.setAdapter(customSpinnerAdapter); // Set the custom adapter to the spinner
                    // You can create an anonymous listener to handle the event when is selected an spinner item
                    binding.spinnerCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view,
                                                   int position, long id) {
                            // Here you get the current item (a User object) that is selected by its position
                            CategoryEntity selectedCategory = customSpinnerAdapter.getItem(position);

                            List<CategoryEntity> childCategoryList = database.getChildCategoryDao().getChildCategoryNameBYCatID(selectedCategory.getId());
                            if (childCategoryList.size() > 0) {
                                int[] categoryId = new int[childCategoryList.size()];
                                for (int i = 0; i < childCategoryList.size(); i++) {
                                    categoryId[i] = childCategoryList.get(i).getId();
                                }
                                getProductsByCatId(categoryId);
                                setChildCategoryVisibility(true, childCategoryList);
                            } else {
                                int[] categoryId = {selectedCategory.getId()};
                                setChildCategoryVisibility(false, childCategoryList);
                                getProductsByCatId(categoryId);


                            }

                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapter) {
                        }
                    });
                }
            });

        });


    }

    private void setChildCategoryVisibility(boolean isVisible, List<CategoryEntity> childCategoryList) {

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (isVisible) {
                    binding.relChildCat.setVisibility(View.VISIBLE);
                    binding.spinnerChildcat.setPrompt("Select your");

                    childCustomSpinnerAdapter = new CustomSpinnerAdapter(getActivity(),
                            android.R.layout.simple_spinner_item,
                            childCategoryList);
                    binding.spinnerChildcat.setAdapter(childCustomSpinnerAdapter);

                    // Set the custom adapter to the spinner
                    // You can create an anonymous listener to handle the event when is selected an spinner item
                    binding.spinnerChildcat.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view,
                                                   int position, long id) {
                            // Here you get the current item (a User object) that is selected by its position
                            CategoryEntity selectedCategory = childCustomSpinnerAdapter.getItem(position);

                            int[] categoryId = {selectedCategory.getId()};

                            //getProductsByCatId(categoryId);

                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapter) {
                        }
                    });
                } else {
                    binding.relChildCat.setVisibility(View.GONE);

                }
            }
        });
    }


    private void getProductsByCatId(int[] catId) {
        appExecutors.getExeDiskIO().execute(() -> {
            productsList = database.getProductDao().getProductsByCatId(catId);


            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (productsList.size() > 0) {

                            resultAdapter = new ResultAdapter(productsList, clickInterface);
                            binding.recyclerview.setLayoutManager(new LinearLayoutManager(getActivity()));
                            binding.recyclerview.setItemAnimator(new DefaultItemAnimator());
                            binding.recyclerview.setAdapter(resultAdapter);

                    }
                }
            });
        });

    }

    @Override
    public void setProduct(ProductEntity productEntity) {
        addFragment(new ProductDetailsFragment(), productEntity);
    }

    private void addFragment(Fragment fragment, ProductEntity productEntity) {
        Bundle bundle = new Bundle();
        bundle.putInt("productId", productEntity.getId());
        bundle.putString("productName", productEntity.getName());
        fragment.setArguments(bundle);
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frameLayout_main, fragment, "ProductDetailsFragment").addToBackStack(null);
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.commitAllowingStateLoss();
    }
}
