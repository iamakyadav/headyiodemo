package com.headyio.ajay.headyiodemo.db.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.headyio.ajay.headyiodemo.db.entities.RankingMappingEntity;

import java.util.List;

import static com.headyio.ajay.headyiodemo.constants.AllConstants.*;

@Dao
public interface RankingMappingDao {

    @Query("Select * from " + TABLE_RANKING_MAPPING)
    List<RankingMappingEntity> getRankingMappingEntity();


    @Insert(onConflict = OnConflictStrategy.IGNORE)
    long insertRankMapping(RankingMappingEntity rankingMappingEntity);

    @Query("Delete from " + TABLE_RANKING_MAPPING)
    void clearRankingMappingData();

}
