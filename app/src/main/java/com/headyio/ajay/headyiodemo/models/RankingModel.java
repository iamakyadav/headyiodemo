package com.headyio.ajay.headyiodemo.models;


import java.util.List;


public class RankingModel {
    private String ranking;
    private List<RankingProductModel> products;


    public String getRanking() {
        return ranking;
    }

    public void setRanking(String ranking) {
        this.ranking = ranking;
    }

    public List<RankingProductModel> getProducts() {
        return products;
    }

    public void setProducts(List<RankingProductModel> products) {
        this.products = products;
    }
}
