package com.headyio.ajay.headyiodemo.db.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.headyio.ajay.headyiodemo.db.entities.TaxEntity;

import java.util.List;

import static com.headyio.ajay.headyiodemo.constants.AllConstants.*;

@Dao
public interface TaxDao {
    @Query("Select * from "+TABLE_TAX)
    List<TaxEntity> getTaxEntity();

    @Insert(onConflict= OnConflictStrategy.IGNORE)
    long insertTax(TaxEntity taxEntity);

    @Query("Delete from " + TABLE_TAX)
    void clearTaxData();


    @Query("Select * from " + TABLE_TAX+" where name=:name and value=:value")
    TaxEntity getTax(String name,double value);

    @Query("Update " + TABLE_DB_STATUS + " set status=:status")
    int updateDBSTatus(int status);
}
