package com.headyio.ajay.headyiodemo;

import android.arch.lifecycle.MediatorLiveData;
import com.headyio.ajay.headyiodemo.db.AppDatabase;
import com.headyio.ajay.headyiodemo.db.entities.CategoryEntity;
import java.util.List;

public class DataRepository {
    private static DataRepository dataRepository;
    private final AppDatabase appDatabase;
    private MediatorLiveData<List<CategoryEntity>> obervableCategoryEntity;

    private DataRepository(AppDatabase myTaskDatabase) {
        appDatabase = myTaskDatabase;
        obervableCategoryEntity = new MediatorLiveData<>();

        obervableCategoryEntity.addSource(appDatabase.getCategoryDao().getCategoryEntity(),
                entityList -> {
                    if (appDatabase.getDatabaseCreated().getValue() != null) {
                        obervableCategoryEntity.postValue(entityList);
                    }
                });
    }

    public static DataRepository getDataRepository(final AppDatabase appDatabase) {
        if (dataRepository == null) {
            synchronized (DataRepository.class) {
                if (dataRepository == null) {
                    dataRepository = new DataRepository(appDatabase);
                }
            }
        }
        return dataRepository;
    }

    public List<CategoryEntity> getCategoryList() {
        return appDatabase.getCategoryDao().getCategoryEntityList();
    }

}
