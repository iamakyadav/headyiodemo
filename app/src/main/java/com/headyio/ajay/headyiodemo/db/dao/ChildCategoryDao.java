package com.headyio.ajay.headyiodemo.db.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.headyio.ajay.headyiodemo.db.entities.CategoryEntity;
import com.headyio.ajay.headyiodemo.db.entities.ChildCategoryEntity;

import java.util.List;

import static com.headyio.ajay.headyiodemo.constants.AllConstants.*;

@Dao
public interface ChildCategoryDao {
    @Query("Select * from " + TABLE_CHILD_CATEGORY)
    List<ChildCategoryEntity> getChildCategoryEntity();

    @Query("Delete from " + TABLE_CHILD_CATEGORY)
    void clearChildCategoryData();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertChildCategory(ChildCategoryEntity childCategoryEntity);

    @Query("select * from " + TABLE_CATEGORY + " where id in (select childCategoryId from " + TABLE_CHILD_CATEGORY + " where categoryID=:catId)")
    List<CategoryEntity> getChildCategoryNameBYCatID(int catId);
}
