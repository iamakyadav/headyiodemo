package com.headyio.ajay.headyiodemo.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.headyio.ajay.headyiodemo.databinding.SpinnerItemBinding;
import com.headyio.ajay.headyiodemo.db.entities.CategoryEntity;
import com.headyio.ajay.headyiodemo.db.entities.RankingEntity;

import java.util.List;

public class RankingSpinnerAdapter extends ArrayAdapter<RankingEntity> {

    private Context context;
    private List<RankingEntity> values;
    private SpinnerItemBinding binding;

    public RankingSpinnerAdapter(Context context, int textViewResourceId,
                                 List<RankingEntity> values) {
        super(context, textViewResourceId, values);
        this.context = context;
        this.values = values;
    }

    @Override
    public int getCount() {
        return values.size();
    }

    @Override
    public RankingEntity getItem(int position) {
        return values.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView label = (TextView) super.getView(position, convertView, parent);
        label.setTextColor(Color.BLACK);
        label.setPadding(15, 15, 15, 15);
             label.setText(values.get(position).getName());

        return label;
    }

    @Override
    public View getDropDownView(int position, View convertView,
                                ViewGroup parent) {
        TextView label = (TextView) super.getDropDownView(position, convertView, parent);
        label.setTextColor(Color.BLACK);
        label.setText(values.get(position).getName());

        return label;
    }
}