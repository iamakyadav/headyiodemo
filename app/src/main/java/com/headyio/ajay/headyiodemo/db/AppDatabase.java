package com.headyio.ajay.headyiodemo.db;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.migration.Migration;
import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;
import com.headyio.ajay.headyiodemo.AppExecutors;
import com.headyio.ajay.headyiodemo.db.dao.CategoryDao;
import com.headyio.ajay.headyiodemo.db.dao.ChildCategoryDao;
import com.headyio.ajay.headyiodemo.db.dao.DBStatusDao;
import com.headyio.ajay.headyiodemo.db.dao.ProductDao;
import com.headyio.ajay.headyiodemo.db.dao.RankingDao;
import com.headyio.ajay.headyiodemo.db.dao.RankingMappingDao;
import com.headyio.ajay.headyiodemo.db.dao.TaxDao;
import com.headyio.ajay.headyiodemo.db.dao.TaxMappingDao;
import com.headyio.ajay.headyiodemo.db.dao.VariantsDao;
import com.headyio.ajay.headyiodemo.db.dao.VariantsMappingDao;
import com.headyio.ajay.headyiodemo.db.entities.CategoryEntity;
import com.headyio.ajay.headyiodemo.db.entities.ChildCategoryEntity;
import com.headyio.ajay.headyiodemo.db.entities.DBStatusEntity;
import com.headyio.ajay.headyiodemo.db.entities.ProductEntity;
import com.headyio.ajay.headyiodemo.db.entities.RankingEntity;
import com.headyio.ajay.headyiodemo.db.entities.RankingMappingEntity;
import com.headyio.ajay.headyiodemo.db.entities.TaxEntity;
import com.headyio.ajay.headyiodemo.db.entities.TaxMappingEntity;
import com.headyio.ajay.headyiodemo.db.entities.VariantsEntity;
import com.headyio.ajay.headyiodemo.db.entities.VariantsMappingEntity;

import static com.headyio.ajay.headyiodemo.constants.AllConstants.*;

@Database(entities = {CategoryEntity.class, ChildCategoryEntity.class,
        ProductEntity.class, RankingEntity.class, RankingMappingEntity.class,
        VariantsEntity.class, VariantsMappingEntity.class, TaxEntity.class,
        TaxMappingEntity.class, DBStatusEntity.class}
        , version = DATABASE_VERSION,exportSchema = false)

public abstract class AppDatabase extends RoomDatabase {
    private static  AppDatabase         appDatabase;
    public abstract CategoryDao         getCategoryDao();
    public abstract ChildCategoryDao    getChildCategoryDao();
    public abstract ProductDao          getProductDao();
    public abstract RankingDao          getRankingDao();
    public abstract RankingMappingDao   getRankingMappingDao();
    public abstract TaxDao              getTaxDao();
    public abstract TaxMappingDao       getTaxMappingDao();
    public abstract VariantsDao         getVariantsDao();
    public abstract VariantsMappingDao  getVariantsMappingDao();
    public abstract DBStatusDao         getDBDbStatusDao();

    private final MutableLiveData<Boolean> isDBCreated = new MutableLiveData<>();

    public static AppDatabase getInstance(final Context context, AppExecutors appExecutors){
        if(appDatabase == null){
            synchronized (AppDatabase.class){
                if(appDatabase == null){
                    appDatabase = buildDatabase(context.getApplicationContext(), appExecutors);
                    appDatabase.updateDatabaseCreated(context.getApplicationContext());
                }
            }
        }
        return appDatabase;
    }

    private static AppDatabase buildDatabase(final Context context, final AppExecutors appExecutors){
        return Room.databaseBuilder(context, AppDatabase.class, DATABASE_NAME)
                /*.addCallback(new Callback() {
                    @Override
                    public void onCreate(@NonNull SupportSQLiteDatabase db) {
                        super.onCreate(db);
                        appExecutors.getExeDiskIO().execute(()->{
                            // Add a delay to simulate a long-running operation
                            addDelay();
                            // Generate the data for pre-population
                            AppDatabase database = AppDatabase.getInstance(context, appExecutors);
                            insertData(database);
                            // notify that the database was created and it's ready to be used
                            database.setDatabaseCreated();
                            Log.e("DB","CREATED");
                        });
                    }
                })*/
               // .addMigrations(MIGRATION_1_2)
                .allowMainThreadQueries()
                .build();
    }

    private void updateDatabaseCreated(final Context context) {
        if (context.getDatabasePath(DATABASE_NAME).exists()) {
            setDatabaseCreated();
        }
    }

    private void setDatabaseCreated(){
        isDBCreated.postValue(true);
    }

    private static void insertData(final AppDatabase database) {
        database.runInTransaction(() -> {
            CategoryEntity entity = new CategoryEntity();
            entity.setId(1);
            entity.setName("Test");
            appDatabase.getCategoryDao().insertCategory(entity);
        });
    }

    private static void addDelay() {
        try {
            Thread.sleep(4000);
        } catch (InterruptedException ignored) {
        }
    }

    //Migrations
    static final Migration MIGRATION_1_2 = new Migration(1, 2) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {
            // Since we didn't alter the table, there's nothing else to do here.
        }
    };

    static final Migration MIGRATION_2_3 = new Migration(2, 3) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {
            database.execSQL("ALTER TABLE "+TABLE_CATEGORY
                    + " ADD COLUMN test TEXT");
            Log.e("DB", "Migrated to 2_3");
        }
    };

    public LiveData<Boolean> getDatabaseCreated() {
        return isDBCreated;
    }
}

