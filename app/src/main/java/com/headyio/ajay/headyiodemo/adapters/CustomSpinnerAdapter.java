package com.headyio.ajay.headyiodemo.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.headyio.ajay.headyiodemo.databinding.SpinnerItemBinding;
import com.headyio.ajay.headyiodemo.db.entities.CategoryEntity;

import java.util.List;

public class CustomSpinnerAdapter extends ArrayAdapter<CategoryEntity> {

    private Context context;
    private List<CategoryEntity> values;
    private SpinnerItemBinding binding;

    public CustomSpinnerAdapter(Context context, int textViewResourceId,
                                List<CategoryEntity> values) {
        super(context, textViewResourceId, values);
        this.context = context;
        this.values = values;
    }

    @Override
    public int getCount() {
        return values.size();
    }

    @Override
    public CategoryEntity getItem(int position) {
        return values.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView label = (TextView) super.getView(position, convertView, parent);
        label.setTextColor(Color.BLACK);
        label.setPadding(15, 15, 15, 15);
        label.setText(values.get(position).getName());
        return label;
    }

    @Override
    public View getDropDownView(int position, View convertView,
                                ViewGroup parent) {
        TextView label = (TextView) super.getDropDownView(position, convertView, parent);
        label.setTextColor(Color.BLACK);
        label.setText(values.get(position).getName());

        return label;
    }
}