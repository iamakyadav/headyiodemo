package com.headyio.ajay.headyiodemo.db.entities;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import static com.headyio.ajay.headyiodemo.constants.AllConstants.TABLE_VARIANT;
import static com.headyio.ajay.headyiodemo.constants.AllConstants.TABLE_VARIANT_MAPPING;

@Entity(tableName = TABLE_VARIANT_MAPPING)
public class VariantsMappingEntity {

    @PrimaryKey(autoGenerate = true)
    private int id;
    private int productId;
    private int variantId;

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getVariantId() {
        return variantId;
    }

    public void setVariantId(int variantId) {
        this.variantId = variantId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
